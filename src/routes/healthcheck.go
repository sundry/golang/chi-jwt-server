package routes

import (
	"net/http"

	"github.com/go-chi/chi"
)

// HealthcheckRouter ...
type HealthcheckRouter struct{}

// Routes ...
func (rs HealthcheckRouter) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", rs.Get)

	return r
}

// Get ...
func (rs HealthcheckRouter) Get(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}
